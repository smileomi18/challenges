
## Practice Problems ##

Use a programming language you are most comfortable wtih and develop a program
to solve at least Part 1 and 2 of the exercise mentioned on this page.

http://codekata.com/kata/kata04-data-munging/

Include sufficient unit tests that can be run independently.
