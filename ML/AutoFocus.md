# AutoFocus in automated microscopy

Implement a CNN as described in this paper.

https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5943362/

Four focus stack images are available in the directories named stack1, stack 2, stack3 and stack4 for use in training and testing.
