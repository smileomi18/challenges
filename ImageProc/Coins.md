
## Loose change ##

In this problem, use your favourite image processing library/framework and
programming language of your choice to mark out the coins in the image coin.jpg.
The output should be an image file with the coins highlighted (e.g. circled in green color).
