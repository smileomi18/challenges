
## Blood Smears ##

smear[1-3].jpg are blood smear sample images. <br/>
A standard blood test involves
counting different kinds of cells by a human looking at smear glass slides
under a microscope. <br />
In this problem, use your favourite image processing library/framework and programming language of choice to detect the number of red blood cells present in the image smear3.jpg and segment out white blood cells (blue colored objects), if present.
<br />
Output should be a count of red blood cells and cropped out images of WBCs.

<br />
(Hint: Use circular hough transform for counting, before counting use image enhancement methods to improve image quality)
