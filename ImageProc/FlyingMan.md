## The Flying Man ##

In this problem, use your favorite image processing library/framework and 
choice of programming language to crop out the man from the image flyingman.jpg.
<br />
Output shall be an image of the man.
