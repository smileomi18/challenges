
## Sharing location over BLE ##

Write an Android application that enables sharing of GPS co-ordinates over
Bluetooth. <br />

The application should enable sending and receiving location 
information from nearby phones.
