## Byomkesh needs to check for equality ##

Byomkesh : Ajit babu, are you game to solve a challenge ? <br/>
Ajit : Do I have a choice ? <br/>
Byomkesh : Ha, ha, hah. Do not feel so resigned. Challenges are good for the little grey cells as Hercule Poirot would say. <br/>
Ajit : I am no Hercule Poirot. Or Byomkesh Bakshi for that matter. <br/>
Byomkesh : Yes. yes. You are the one and only Ajit Kumar Mitra ! <br />
Ajit : Ok. Let us hear your challenge. <br/>

Byomkesh : I have this string of characters. The string has only two types of characters - either 'A' or 'B'. <br/>
They can be repeated any number of times. I need to find out if the string has equal number of 'A's and 'B's or not. <br/>

Ajit : This is easy for a change ! Just iterate through the string, count the number of each character and compare at the end. <br/>
Byomkesh : Not so fast my friend. There are constraints. <br/>
Ajit : Of course. Nothing comes without constraints. <br/>
Byomkesh : You cannot use a counter. Nor can you find out the length of the string. <br/>
Ajit : Now that is a challenge.<br/>
Byomkesh : Question is can you solve it ? <br/>

Write a clear description of how you would help Ajit solve this challenge. Code is not needed for this challenge. <br/>
Please use clear language to articulate the steps/methods that would be used to arrive at the final answer.