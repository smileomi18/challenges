## Challenges ##

Programming problems, logical problems, problems. Period. <br />
These problems are meant to assess various skills for recruiting great talent for our projects. <br />

Clone the repo. Work on the problem(s) mentioned in the communication to you and send back a link to your solution. <br />

Good Luck ! <br />
:-) <br />
Team ShanMukha 