
# Task 1 #

## Part (a) ##

A low-impedant sensor outputs a 100 Hz sine wave which has a peak-to-peak amplitude ranging from 10 mv to 100 mv (depending on the the parameter being sensed - ignore that for now). A 4.0 V DC offset is superimposed on this output

1. Draw the input waveform
2. Draw a circuit for extracting this sine wave and amplifying it so that it can be fed to a 8-bit ADC with Vref of 5V. Also draw the output of this circuit.

Note : You can provide hand-drawn design for this part. No need to provide part number for the op-amps and other electronic components but please label all components. A short explanation on the working of the circuit is expected.


## Part(b) ##
Assuming that output of your circuit in part(a) is being fed to A0 pin of an Arduino, develop a program to :

1. Convert the analog to a digital value with a conversion time satisfying Nyquist criteria. Name this function “measureADC”

2. Accept the following two user inputs via COM port (at baud 9600):
 “w” to do one ADC conversion and write the value at location “0” of flash memory
 “r” to read the value at location “0” of flash memory
The functions should be named “f_write”: and “f_read”

Note: Don't use open source library for read and write operations in flash memory in this. 

# Task2 #
Repeat part (a) of Task 1 but with the sensor being high-impedant (max current of 1 micro Amp). 
Everything else remains same. No need to repeat part (b)
